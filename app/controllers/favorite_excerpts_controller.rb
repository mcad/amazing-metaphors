class FavoriteExcerptsController < ApplicationController
  before_action :set_excerpt
  before_action :logged_in_user, only: [:create, :destroy]
  
  def create
    if Favorite.create(favorited: @excerpt, user: current_user)
      respond_to do |format|
        format.html { 
          flash[:success] = 'Excerpt added to your favorites'
          redirect_to @excerpt 
        }
        format.js
      end
    else
      redirect_to @excerpt, alert: 'Something went wrong...'
    end
  end
  
  def destroy
    Favorite.where(favorited_id: @excerpt.id, user_id: current_user.id).first.destroy
    respond_to do |format|
  	  format.html { 
        flash[:success] = 'Excerpt is no longer a favorite'
        redirect_to request.referrer || root_url
      }
      format.js
    end
  end
  
  private
  
  def set_excerpt
    @excerpt = Micropost.find(params[:micropost_id] || params[:id])
  end
end