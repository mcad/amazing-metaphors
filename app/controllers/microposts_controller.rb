class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:destroy, :update, :edit]
  before_action :correct_user,   only: :destroy
  def show
    @quote = Micropost.find(params[:id])
    @nextrandom = Micropost.offset(rand(Micropost.count)).first
    
    set_meta_tags :site => 'Amazing Metaphors - The Best Metaphors and Similes',
              :publisher => 'https://plus.google.com/110072472091956031632',
              :title => @quote.author,
              :description => "Quote from #{@quote.author} - #{@quote.content}",
              :keywords => "#{@quote.author}, #{@quote.content}, metaphor, simile, best metaphors, best similes, excerpt, quote, literature, art",
              :reverse => true,
              :og => {
              :site_name => 'Amazing Metaphors',
              :title    => "#{@quote.comparison_type} by #{@quote.author.upcase}",
              :description     => "#{@quote.content}",
              :url      => "https://amazingmetaphors.com#{view_context.url_for @quote}",
              :image    => "#{view_context.image_url('AMlogo.png')}"},
              :fb => {
                :app_id => '421096071401226'
              }
  end
  
  def index
    @q = Micropost.ransack(params[:q])
    @search = Micropost.search(params[:q])
    if params[:tag]
      @quotes = Micropost.tagged_with(params[:tag]).paginate(page: params[:page])
    elsif params[:metaphor]
      @quotes = Micropost.where(:comparison_type => 'Metaphor').paginate(page: params[:page])
    elsif params[:simile]
      @quotes = Micropost.where(:comparison_type => 'Simile').paginate(page: params[:page])
    else
      @quotes = @q.result(distinct: true).page(params[:page])
    end
    
    set_meta_tags :site => 'Amazing Metaphors - The Best Metaphors and Similes',
              :publisher => 'https://plus.google.com/110072472091956031632',
              :title => 'Excerpts',
              :description => "Excerpts of amazing metaphors and similes",
              :keywords => "metaphor, simile, best metaphors, best similes, excerpt, quote, literature, art",
              :reverse => true,
              :og => {
              :site_name => 'Amazing Metaphors',
              :title    => "Recently Added",
              :description     => "Our most recently added metaphors and similes.",
              :url      => 'https://www.amazingmetaphors.com/',
              :image    => "#{view_context.image_url('AMlogo.png')}"},
              :fb => {
                :app_id => '421096071401226'
              }
  end
  
  def create
    if logged_in?
      @micropost = current_user.microposts.build(micropost_params)
    else
      @micropost = User.where(:email => 'guest@amazingmetaphors.com').first.microposts.build(micropost_params)
    end
    if @micropost.save
      flash[:success] = "Submission successful!"
      redirect_to @micropost
    else
      render 'new'
    end
    set_meta_tags :noindex => true
  end
  
  def new
    @micropost = Micropost.new
    
    set_meta_tags :site => 'Amazing Metaphors - The Best Metaphors and Similes',
              :publisher => 'https://plus.google.com/110072472091956031632',
              :title => 'New Submission',
              :description => "Submit an amazing metaphor or simile",
              :keywords => "metaphor, simile, best metaphors, best similes, excerpt, quote, literature, art",
              :nofollow => true,
              :reverse => true
  end
  
  def edit
    set_meta_tags :noindex => true
    @micropost = Micropost.find(params[:id])
  end
  
  def update
    set_meta_tags :noindex => true
    @micropost = Micropost.find(params[:id])
    if @micropost.update_attributes(micropost_params)
      flash[:success] = "Successfully updated"
      redirect_to @micropost
    else
      render 'edit'
    end
  end

  def destroy
    set_meta_tags :noindex => true
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    redirect_to root_url
  end

  private

    def micropost_params
      params.require(:micropost).permit(:content, :comparison_type, :author, :name_of_full_work, :link_to_full_work, :year, :tag_list)
    end
    
    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
