class StaticPagesController < ApplicationController
  def home
    quotes = Micropost.reorder("RANDOM()").limit(10)
    @fpquote = quotes[0]
    @morequotes = quotes[1..-1]
    @nextrandom = Micropost.offset(rand(Micropost.count)).first
    
    set_meta_tags :site => 'Amazing Metaphors - The Best Metaphors and Similes',
              :publisher => 'https://plus.google.com/110072472091956031632',
              :description => "Amazing Metaphors has the best similes and metaphors from you favorite artists and authors",
              :keywords => "metaphor, simile, best metaphors, best similes, excerpt, quote, literature, art",
              :og => {
              :site_name => 'Amazing Metaphors',
              :title    => "Amazing Metaphors",
              :description     => "Find and share the best metaphors and similes from your favorites authors and artists.",
              :url      => 'https://www.amazingmetaphors.com/',
              :image    => "#{view_context.image_url('AMlogo.png')}"},
              :fb => {
                :app_id => '421096071401226'
              }

    #use for user profile feed
    # if logged_in?
    #   @micropost = current_user.microposts.build
    #   @feed_items = current_user.feed.paginate(page: params[:page])
    # else
    #   @quotes = Micropost.paginate(page: params[:page])
    # end
  end

  def tos
    set_meta_tags :site => 'Amazing Metaphors - The Best Metaphors and Similes',
              :publisher => 'https://plus.google.com/110072472091956031632',
              :title => 'Terms of Service'
  end
end
