class SessionsController < ApplicationController
  def new
    set_meta_tags :title => 'Sign in',
              :description => "Sign in to your Amazing Metaphors account",
              :keywords => "amazing metaphors, sign in, log in",
              :reverse => true
  end
  
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      remember user
      redirect_back_or root_url
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
