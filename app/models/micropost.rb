class Micropost < ActiveRecord::Base
  has_many :favorites, as: :favorited
  has_many :fans, through: :favorites, source: :user
  acts_as_taggable
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { minimum: 9, maximum: 340 }
  validates :comparison_type, presence: true
  validates :author, presence: true, length: { minimum: 3, maximum: 40 }
  validates :name_of_full_work, length: { maximum: 100 }, allow_blank: true
  validates :link_to_full_work, length: { maximum: 200 }, allow_blank: true
  validates :year, length: { maximum: 4 }, allow_blank: true
  self.per_page = 10
  
  def slug
    author.downcase.gsub(" ", "-").gsub(".","")  
  end

  def to_param
    "#{id}-#{slug}"
  end
end
