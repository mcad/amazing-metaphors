SitemapGenerator::Sitemap.default_host = "https://www.amazingmetaphors.com"
SitemapGenerator::Sitemap.sitemaps_host = "http://s3.amazonaws.com/sitemap-generator/"
SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'
SitemapGenerator::Sitemap.adapter = SitemapGenerator::WaveAdapter.new
SitemapGenerator::Sitemap.create do
  Micropost.find_each do |micropost|
    add micropost_path(micropost), :lastmod => micropost.updated_at
  end
end