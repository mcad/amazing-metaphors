class CreateMicroposts < ActiveRecord::Migration
  def change
    create_table :microposts do |t|
      t.text :content
      t.string :comparison_type
      t.string :author
      t.string :name_of_full_work
      t.string :link_to_full_work
      t.integer :year
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :microposts, :users
    add_index :microposts, [:user_id, :created_at]
  end
end
